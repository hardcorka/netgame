require 'include/class'

local GameManager = class(function (self)
	self.MAIN_MENU = 0
	self.PAUSE = 1
	self.INGAME = 2
	self.INGAME_NET = 3
	self.GAMEOVER = 1337
	
	self.prevState = 0
	self.state = 0
end)

function GameManager:setGameInstance(inst)
	self.game = inst
end

function GameManager:isMainMenu()
	return self.state == self.MAIN_MENU
end

function GameManager:isPause()
	return self.state == self.PAUSE
end

function GameManager:isIngame()
	return self.state == self.INGAME or self.state == self.INGAME_NET
end

function GameManager:isIngameNet()
	return self.state == self.INGAME_NET
end

function GameManager:pause()
	self.prevState = self.state
	self.state = self.PAUSE
end

function GameManager:goToMainMenu()
	self.prevState = self.state
	self.state = self.MAIN_MENU
end

function GameManager:resumeGame()
	tmp = self.state
	self.state = self.prevState
	self.prevState = tmp
end

function GameManager:newGame()
	self.prevState = self.state
	self.state = self.INGAME
	if self.game then
		self.game:init()
	end
end

function GameManager:startNetGame(ip, port)
	self.prevState = self.state
	self.game.client:connect(ip, port)
	self.game:initMultiplayer()
	self.state = self.INGAME_NET
end

return GameManager