require 'include/class'

local NetPlayer = class(function (self, world, id)
	self.world = world
	self.x = 1100
	self.y = 800
	self.id = id
	self.width = 100
	self.height = 100
	self.runSpeed = 300
	self.maxSpeed = 2000
	self.lives = 3

	world:registerPlayer(self)
end)

function NetPlayer:update(dt)
	-- SOCKET COMMAND GET X, Y AND LIVES FROM SERVER
end

function NetPlayer:draw()
	love.graphics.rectangle("line", self.x, self.y, self.width, self.height)
end

function NetPlayer:input(key)
	if key == 'w' or key == ' ' then
		-- SOCKET COMMAND - JUMP
	end

	if key == 'a' then
		-- SOCKET COMMAND - RUN/IDLE
	end

	if key == 's' then
		-- SOCKET COMMAND - SLOW
	end
end

return NetPlayer