require 'include/class'

local World = class(function (self, gravity, width)
	self.ceil = 600
	self.floor = 1200
	self.gravity = gravity or 9.8
	self.objects = {}
	self.players = {}
end)

function World:setLength(length)
	self.length = length
	self.ground = {
		0, self.floor,
		length, self.floor
	}
	self.sky = {
		0, self.ceil,
		length, self.ceil	
	}
end

function World:registerObject(object)
	table.insert(self.objects, object)
end

function World:registerPlayer(player)
	table.insert(self.players, player)
end

function World:unregisterPlayer(player)
	for k, v in ipairs(self.players) do
		if v == player then
			table.remove(self.players, k)
		end
	end
end

function World:unregisterObject(object)
	for k, v in ipairs(self.objects) do
		if v == player then
			table.remove(self.objects, k)
		end
	end
end

function World:update(dt)
	for _, p in ipairs(self.players) do
		self:applyGravity(p, dt)
		local lose = false
		if p.state == 0 and p.runSpeed < p.maxSpeed then
			p.runSpeed = p.runSpeed + 20 * dt
		end
		if not p:isCooldown() then
			for __, o in ipairs(self.objects) do
				if p:hitTest(o) then
					p:loseLive()
					p.runSpeed = 300
					if o.y + o.height == self.floor then
						p.y = o.y - p.height - 25
						p.impulse = 200
					elseif o.y == self.ceil then
						p.y = o.y + o.height + 25
						p.impulse = 0
					end
				end
				if p.y < self.ceil then
					p.y = self.ceil
				end
			end
		end
	end

	for _, o in ipairs(self.objects) do
		self:applyGravity(o, dt)
	end
end

function World:draw()
	for _, p in ipairs(self.objects) do
		p:draw()
	end
	love.graphics.line(self.ground)
	love.graphics.line(self.sky)
end

function World:applyGravity(object, dt)
	if object.y + object.height < self.floor then
		object.y = object.y + object.mass / self.gravity*self.gravity*3*dt
		if object.y + object.height > self.floor then
			object.y = self.floor - object.height
		end
	end
end

function World:hitTest(a, b)
	return 	a.x < b.x + b.width and
					b.x < a.x + a.width and
					a.y < b.y + b.height and
					b.y < a.y + a.height
end

return World