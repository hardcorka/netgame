require 'include/class'

local Object = class(function (self, world, x, y, mass, width, height)
	self.world = world
	self.x = x or 0
	self.y = y or 0
	self.mass = mass or 80
	self.width = width or 100
	self.height = height or 100
	
	world:registerObject(self)
end)

function Object:draw()
	love.graphics.rectangle("line", self.x, self.y, self.width, self.height)
end

return Object