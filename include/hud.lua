require 'include/class'

local HUD = class(function (self, x, y, player)
	self.x = x
	self.y = y
	self.player = player
	self.width = 80
	self.height = 10
end)

function HUD:update(dt)
	
end

function HUD:draw()
	love.graphics.rectangle("line", self.x, self.y, self.width, self.height)
	love.graphics.rectangle("fill", self.x, self.y, self.player.runSpeed / self.player.maxSpeed * self.width , self.height)
	love.graphics.printf("Скорость", self.x + self.width /2, self.y + self.height + 5, 0, 'center')

	love.graphics.setColor(255, 0, 0, 255)
	for i = 0, self.player.lives-1, 1 do
		love.graphics.rectangle("fill", 20 + self.x + self.width + (i * 20), self.y, 10, 10)
	end
	love.graphics.setColor(255, 255, 255, 255)
	love.graphics.printf("Жизни", 20 + self.x + self.width + 25, self.y + self.height + 5, 0, 'center')

	love.graphics.line(100, 535, 700, 535)
	love.graphics.setColor(255, 255, 255, 255)
	for k, v in ipairs(self.player.world.players) do
		if self.player.id ~= v.id then
			love.graphics.setColor(255 - k * 20, 255 - k * 20, 255 - k * 20, 255)
		end
		love.graphics.rectangle("fill", 
			100 - 7 + (v.x / self.player.world.length) * 600, 
			532, 
			7, 7
		)
		love.graphics.setColor(255, 255, 255, 255)
	end

end

return HUD