require 'include/class'

local Player = class(function (self, world, x, y, mass, id)
	self.id = id or 0
	self.x = x or 0
	self.y = y or 0
	self.mass = mass or 80
	self.world = world
	self.impulse = 0
	self.runSpeed = 900
	self.maxSpeed = 2000
	self.state = 0
	self.lives = 3
	self.lastCollide = love.timer.getTime()
	
	self.body = {}
	self.body.fuselage = {
		width = 100,
		height = 100
	}

	-- self.body.nose = {
	-- 	width = 15,
	-- 	height = 70,
	-- 	offsetX = 100,
	-- 	offsetY = 15
	-- }

	self.width = 100
	self.height = 100

	self.texture = love.graphics.newImage("textures/ship.png")
	self.particle = love.graphics.newImage("textures/whiteOrb.png")

	world:registerPlayer(self)
end)

function Player:loseLive()
	if self.lives > 0 then
		self.lives = self.lives - 1
	end
end

function Player:setCooldown()
	self.lastCollide = love.timer.getTime()
end

function Player:isCooldown()
	-- print(love.timer.getTime().." and "..self.lastCollide)
	return love.timer.getTime() - self.lastCollide <= 0.33
end

function Player:jump()
	self.impulse = self.world.gravity * (self.mass * 1.4)
end

function Player:slow()
	self.runSpeed = self.runSpeed - 50
end

function Player:increaseSpeed()
	self.runSpeed = self.runSpeed + self.runSpeed / 100
end

function Player:lowerSpeed()
	self.runSpeed = self.runSpeed - self.runSpeed / 100
end

function Player:input(key)
	if key == 'w' then
		self:jump()
	end

	if key == 'a' then
		self:switchMovement()
	end

	if key == ' ' then
		self:jump()
	end
	if key == 's' then
		self:slow()
	end
end

function Player:switchMovement()
	if self.state == 0 then
		self.state = 1 -- 1 - idling
	else 
		self.state = 0 -- 0 - running
	end
end

function Player:stop()
	self.state = 1
end

function Player:run(dt)
	self.x = self.x + self.runSpeed * dt
end

function Player:update(dt)
	if (self.state == 0) then -- 0 - running
		self:run(dt)
	end
	self:applyImpulse()
end

function Player:applyImpulse()
	if self.impulse > 0 then
		di = self.mass * 1.3
		self.y = self.y - di / self.world.gravity
		self.impulse = self.impulse - di
		if (self.impulse < 0) then 
			self.impulse = 0 
		end
	end
end

function Player:hitTest(object)
	for _, v in pairs(self.body) do
		if self:hitPart(v, object) then
			return true
		end
	end

	return false
end

function Player:hitPart(part, object) 
	return 	self.x + (part.offsetX or 0) < object.x + object.width and
					object.x < self.x + (part.offsetX or 0) + part.width and
					self.y + (part.offsetY or 0) < object.y + object.height and
					object.y < self.y + (part.offsetY or 0) + part.height
end

function Player:draw()
	for k, v in pairs(self.body) do
		self:drawPart(v)
	end
end

function Player:drawPart(part)
	love.graphics.rectangle(
		"line", 
		self.x + (part.offsetX or 0),
		self.y + (part.offsetY or 0),
		part.width,
		part.height
	)
end

return Player