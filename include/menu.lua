require 'include/class'

local lw = love.window
local Menu = class(function (self, manager)
	self.manager = manager

	self.font = love.graphics.newFont("fonts/OpenSans-Regular.ttf", 18) -- Шрифт для пунктов
	self.title = love.graphics.newFont("fonts/OpenSans-Regular.ttf", 30) -- Шрифт для текста

	self.menu = { activePage = 1 }

	self:newMenuItem(1,
		{ text = "Главное меню", x = 200, y = 30 },
		{ "Новая игра", "Сетевая игра", "Выход" },
		{ 30, 30, 30 },
		{ 100, 130, 500 }
	)
	self:newMenuItem(2,
		{ text = "Пауза", x = 200, y = 30 },
		{ "Продолжить", "Главное меню"},
		{ 30, 30 },
		{ 100, 500 }
	)
	self:newMenuItem(3, 
		{ text = "Сетевая игра", x = 200, y = 30 },
		{ 
			"Подключиться к 127.0.0.1", 
			"",
			"Главное меню" 
		},
		{ 30, 30, 30 },
		{ 100, 130, 500 }
	)
end)

function Menu:newMenuItem(index, title, text, textX, textY)
	local menu = { 
		title = title, 
		text = text,
		textX = textX,
		textY = textY,
		activeButton = 0
	}
	
	self.menu[index] = menu
end

function  Menu:input(key)
	local page = self.menu.activePage

	if key == 'w' or key == 'up' then
		self.menu[page].activeButton = self.menu[page].activeButton - 1
	elseif key == 's' or key == 'down' then
		self.menu[page].activeButton = self.menu[page].activeButton + 1
	end

	if self.menu[page].activeButton < 1 then 
		self.menu[page].activeButton = #self.menu[page].text 
	end

	if self.menu[page].activeButton > #self.menu[page].text then 
		self.menu[page].activeButton = 1 
	end

	if key == 'return' or key == ' ' then
		self:buttonPressed()
	end
end

function Menu:update(dt)
	local msXPrev, msYPrev = msX, msY
	local msX, msY = love.mouse.getPosition()
	if msXPrev ~= msX or msYPrev ~= msY then
		local page = self.menu.activePage
		for i = 1, #self.menu[page].text do
			local tw = self.font:getWidth(self.menu[page].text[i])
			local th = self.font:getHeight(self.menu[page].text[i])
			if msX >= self.menu[page].textX[i] and msY >= self.menu[page].textY[i] and
				 msX <= self.menu[page].textX[i] + tw and msY <= self.menu[page].textY[i] + th then
				self.menu[page].activeButton = i
			end
		end
	end
end

function Menu:draw()
	local oldFont = love.graphics.getFont()
	local r, g, b, a = love.graphics.getColor()
	local page = self.menu.activePage
	local menu = self.menu[page]
	local button = menu.activeButton

	love.graphics.setFont(self.title)
	love.graphics.setColor(255, 255, 0, 255)
	love.graphics.printf(menu.title.text, 0, menu.title.y, 800, 'center')
	love.graphics.setFont(self.font)

	for i = 1, #menu.text do
		love.graphics.setColor(255, 255, 255, 255)
		if i == button then 
			love.graphics.setColor(0, 255, 0, 255) 
		end
		love.graphics.print(menu.text[i], menu.textX[i], menu.textY[i], 0, 1, 1)
	end

	love.graphics.setColor(r, g, b, a)
	love.graphics.setFont(oldFont)
end

function Menu:buttonPressed(x, y, b)
	-- Активное меню и нажатая кнопка
	local page = self.menu.activePage
	local button = self.menu[page].activeButton
	if b ~= 'l' then return end
	if page == 1 then 				-- Главное меню
		if button == 1 then 			-- > Новая игра
			self.menu.activePage = 2
			self.manager:newGame()
		elseif button == 2 then 		-- > Сетевая игра
			self.menu.activePage = 3
		elseif button == 3 then 		-- > Выход
			love.event.quit()
		end
	elseif page == 2 then 			-- Меню паузы
		if button == 1 then 			-- > Продолжить
			self.manager:resumeGame()
		elseif button == 2 then 		-- > Главное меню
			self.menu.activePage = 1 
		end
	elseif page == 3 then 			-- Меню сетевой игры
		if button == 1 then 				-- > Подключиться к pstn
			self.manager:startNetGame("127.0.0.1", 55000)
		elseif button == 2 then 			-- > Подключиться к hardcore
			-- self.manager:startNetGame("25.166.1.43", 55000)
		elseif button == 3 then 			-- > Главное меню
			self.menu.activePage = 1 
		end
	end
end

function Menu:textinput(text)
	-- body
end

return Menu
