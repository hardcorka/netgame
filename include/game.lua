require 'include/class'

local Camera = require('include/camera')
local Player = require('include/player')
local World = require('include/world')
local Object = require('include/object')
local Menu = require('include/menu')
local GameManager = require('include/gameManager')
local Track = require('include/track')
local HUD = require('include/hud')

-- net stuff
local Client = require('include/multiplayer')
local NetWorld = require('include/networld')
local NetPlayer = require('include/netplayer')
local OtherPlayer = require('include/otherplayer')

local Game = class(function (self, manager)
	self.manager = manager
	self.menu = Menu(manager)
	self.debug = false
	self.client = Client()
	self:init()

	love.graphics.setFont(
		love.graphics.newFont("fonts/OpenSans-Regular.ttf", 14)
	)

	manager:setGameInstance(self)
end)

function Game:init()
	self.world = World()
	self.player = Player(self.world, 50, 900)
	self.camera = Camera()
	self.hud = HUD(20, 20, self.player)

	self.camera:setBounds(0, self.world.ceil - 100, self.length, self.world.ceil - 100)
	self.camera:setScale(1.5, 1.5)

	self:buildRandomTrack()
end

function Game:initMultiplayer()
	self.world = NetWorld()
	self.player = NetPlayer(self.world, id)

	self.camera = Camera()
	self.hud = HUD(20, 20, self.player)

	self.camera:setBounds(0, self.world.ceil - 100, self.length, self.world.ceil - 100)
	self.camera:setScale(1.5, 1.5)
end

function Game:buildRandomTrack()
	local track = Track(self.world)
	self.length, objects = track:build()
	self.world:setLength(self.length)
end

function Game:update(dt)
	if self.manager:isIngame() and self.player.lives > 0 then
		if self.manager:isIngameNet() then
			self.client:update(dt, self.player,self.world)
		end

		if not self.manager:isIngameNet() then
			self.world:update(dt)
		end
		self.player:update(dt)
		self.camera:setPosition(self.player.x - self.player.width / 2 - 25, self.player.y - 500)
		
	end

	if self.manager:isMainMenu() or self.manager:isPause() then
		self.menu:update(dt)
	end
end

function Game:draw()
	if self.manager:isMainMenu() or self.manager:isPause() then
		self.menu:draw()
	end

	if self.manager:isIngame() then
		if self.player.lives > 0 then
			self.camera:set()
			self.world:draw()
			self.player:draw()
			self.camera:unset()

			self.hud:draw()

			if self.manager:isIngameNet() then
				self.client:draw()
			end
		else
			love.graphics.printf("Ваш результат: "..math.floor(self.player.x), 200, 300, 400, 'center')
		end
	end

	if self.manager:isIngame() and self.debug then
		self:drawDebug()
	end
end

function Game:mousepressed(x, y, b)
	if self.manager:isMainMenu() or self.manager:isPause() then
		self.menu:buttonPressed(x, y, b)
	end
end

function Game:keypressed(key)
	if key == 'f12' then
		self.debug = not self.debug
	end

	if self.manager:isMainMenu() then 
		self.menu:input(key)
	end

	if self.manager:isIngame() and not self.manager:isIngameNet() then
		if key == 'escape' then 
			self.manager:goToMainMenu()
		end
		self.player:input(key)
	end

	if self.manager:isIngameNet() then
		if key == 'escape' then 
			self.manager:pause()
		end
		self.client:input(key)
	end
end

function Game:textinput(text)
	self.menu:textinput(text)
end

function Game:drawDebug()
	love.graphics.printf(love.timer.getFPS(), 790, 5, 0, 'right')

	love.graphics.printf(#self.world.players, 790, 425, 0, 'right')

	love.graphics.printf('Camera', 790, 440, 0, 'right')
	
	love.graphics.printf(self.camera._x, 790, 455, 0, 'right')
	love.graphics.printf(self.camera._y, 790, 470, 0, 'right')
	
	love.graphics.printf('Player', 790, 485, 0, 'right')
	
	love.graphics.printf(self.player.x, 790, 500, 0, 'right')
	love.graphics.printf(self.player.y, 790, 515, 0, 'right')
	love.graphics.printf(self.player.runSpeed, 790, 530, 0, 'right')
	-- love.graphics.printf(self.menu.menu[self.menu.menu.activePage].activeButton, 790, 530, 0, 'right')
end

return Game