﻿require 'include/class'
local OtherPlayer = require('include/otherplayer')
local trackId = nil
local socket = require "socket"
local isOk = false
local udp = socket.udp()
local updateSock = socket.udp()
local x, y, cmd, id
-- Временно это будет константой
-- local address, port = "127.0.0.1", 1337
-- Время ожидания для следующего запроса
local updaterate = 0.03
local timer
local readyToDraw = false
local tmpBool = false 
local CONNECT, JUMP, SWITCH, QUIT, PING = 0, 1, 2, 3, 4
local READY, UPDATE, INFO, NEWPLAYER, GETPLAYERS, LOSTPLAYER = 10, 20, 30, 40, 50, 60

local Client = class(function (self, gamePlayer)
	math.randomseed(os.time())

	-- Уникальный айди юзера задается при подключении к серверу
	self.id = tostring(math.random(99999))
 	self.udp = socket.udp()
 	self.updateSock = socket.udp()
 	self.data = ""
 	self.lastPacketSuccess = false
 	self.timer = 0
 	self.updaterate = 0.1
 	self.time = 0

 	self.trackId = nil
 	self.worldSetup = false
end)

function Client:load()
	-- self.udp:settimeout(0) -- убираем таймаут
	-- self.udp:setpeername(address, port) -- установка параметров

	-- self.updateSock:settimeout(0) -- убираем таймаут
	-- self.updateSock:setpeername(ip, 66000) -- установка параметров

	self.timer = 0
end


function Client:connect(ip, port, gameWorld)
	self.udp = socket.udp()
 	self.updateSock = socket.udp()
	-- ip = "127.0.0.1"
	self.udp:settimeout(0) -- убираем таймаут
	self.udp:setpeername(ip, port) -- установка параметров
 
	self.updateSock:settimeout(0) -- убираем таймаут
	self.updateSock:setpeername(ip, 66000) -- установка параметров

	tim = 0

	self.id = tostring(math.random(99999))
	self.trackId = nil
	self.worldSetup = false

	local dg = string.format("%d %s", CONNECT, self.id)
	self.udp:send(dg)
	repeat

		messs, errs = self.udp:receive()
		if messs then
			tmpMess = messs:split(":")
			cmdGeneral, asdas = tonumber(tmpMess[1])
			if(cmdGeneral == CONNECT) then
				tmpBool = true
				print("successful connection to general")
			end
		end
		-- tim > 1000 and time + 0.0001 == 10 seconds
		if tim > 1500 then 
			error("Network error")
		end

		tim = tim + 0.0001

	until tmpBool
	
	tim = 0

	local dgg = string.format("%d %s", CONNECT, self.id)
	self.updateSock:send(dgg)
	repeat
		if tmpBool then
			local dgg = string.format("%d %s", CONNECT, self.id)
			self.updateSock:send(dgg)

			messss,errs = self.updateSock:receive()
			if  messss then
				tmpMess = messss:split(":")
				cmdUpdate, self.trackId = tonumber(tmpMess[1]), tonumber(tmpMess[2])
				if self.trackId ~= nil then
					trackId = self.trackId
				end
				print("GOT ID", self.trackId)
				if(cmdUpdate == CONNECT) then
					self.lastPacketSuccess = true
					print("successful connection to update")
				end
			end
	
			if tim > 1500 then 
				error("Network error")
			end
			tim = tim + 0.0001
		end
	until self.lastPacketSuccess



	local dgg = string.format("%d %s", PING, self.id)
	self.updateSock:send(dgg)

	self.timer = 0
end

-- connect, players, track 
function Client:update(dt, gamePlayer, gameWorld)
	
	gamePlayer.id = self.id
	self.timer = self.timer + dt

	if trackId ~= nil and not self.worldSetup then
		gameWorld:generateRandomTrack(trackId)
		self.worldSetup = true
	end

	if self.timer > self.updaterate and gamePlayer.lives > 0 then
		local dgrgm = string.format("%d %s %s %s", PING, self.id, gamePlayer.x, gamePlayer.y)
		self.updateSock:send(dgrgm)
		if self.lastPacketSuccess then 
			repeat
				tim = tim + 1
				data, msg = self.updateSock:receive()
				if data then 
					-- print(data)
					local args = data:split(":")
					local title, text = tonumber(args[1]), args[2]
					
					if (title == PING) then
						text = args[2]:split(";")
						id, x, y, runSpeed, lives = text[1]:match("(%d*) (%g*) (%g*) (%g*) (%d*)")
						
						gamePlayer.x, gamePlayer.y = tonumber(x), tonumber(y)
						gamePlayer.runSpeed, gamePlayer.lives = tonumber(runSpeed), tonumber(lives)
						coords, missed = {}, {}
						for k, v in ipairs(text) do
							id, x, y = v:match("(%d*) (%g*) (%g*)")
							if id ~= self.id then
								gameWorld:updatePlayer(id, x, y)
							end
						end

					elseif title == NEWPLAYER then
						other = OtherPlayer(tonumber(text))
						gameWorld:registerPlayer(other)
						dgrgm = string.format("%d %s %s", NEWPLAYER, self.id, text)
						self.updateSock:send(dgrgm)
					elseif title == GETPLAYERS then
						other = OtherPlayer(tonumber(text))
						gameWorld:registerPlayer(other)
					end

					readyToDraw = true
					tim = 0
				elseif msg ~='timeout' then
					-- love.event.quit()
				-- 	error("Network error: "..tostring(msg))
				end
			until not data	
		end
		self.timer = self.timer - dt
	end
end

function Client:draw()
	if readyToDraw then
		-- love.graphics.printf(self.data, 550, 530, 0, 'right')
		-- love.graphics.printf(self.id, 550, 545, 0, 'right')
		-- love.graphics.printf(x,  550, 560, 0, 'right')
		-- love.graphics.printf(y, 550, 575, 0, 'right')
	end
end


function Client:input(key)
	if key == 'w' then 
		local dgrgm = string.format("%s %s", tostring(JUMP), self.id)
		self.updateSock:send(dgrgm)
	end

	if key == 'a' then
		local dgrgm = string.format("%s %s", tostring(SWITCH), self.id)
		self.updateSock:send(dgrgm)
	end

	if key == 's' then

	end
end


function string:split(sep)
        local sep, fields = sep or ":", {}
        local pattern = string.format("([^%s]+)", sep)
        self:gsub(pattern, function(c) fields[#fields+1] = c end)
        return fields
end

function Client:getPlrs(gameWorld)
repeat
	getpl,errs = self.updateSock:receive()
	if getpl then 
		tmpMess = getpl:split(":")
		if(tonumber(tmpMess[1]) == GETPLAYERS) then
			local arrayID = tmpMess[2]:split(";")
			for k, v in ipairs(arrayID) do
				if tonumber(v) ~= self.id then
					other = OtherPlayer(tonumber(v))
					gameWorld:registerPlayer(other)
				end
			end
		end
	end
	until not getpl
	--
end

return Client