require 'include/class'

local OtherPlayer = class(function (self, id)
	self.id = id
	self.x = 1100
	self.y = 800
	self.width = 100
	self.height = 100
end)

function OtherPlayer:update(dt, x, y)
	-- socket stuff
	-- x = SOCKET.x
	-- y = SOCKET.y
	self.x = x
	self.y = y
end

function OtherPlayer:draw()
	love.graphics.rectangle("line", self.x, self.y, self.width, self.height)
	love.graphics.print(
		tostring(self.id), 
		self.x + self.width / 2, 
		self.y - 20
	)

end

return OtherPlayer