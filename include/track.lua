require 'include/class'

local Object = require('include/object')

local Track = class(function (self, world, startX)
	self.seed = nil
	self.objects = {}
	self.world = world
	self.ceil = world.ceil
	self.floor = world.floor
	self.height = math.abs(self.floor - self.ceil)
	self.startX = startX or 1000
end)

function Track:build()
	length = 100000
	segments = 25
	typeSeg, segLength = nil, length / segments
	startSeg = self.startX

	step = 200
	for i = self.startX, self.startX + length, segLength do
		typeSeg = love.math.random(0, 2)
		
		if typeSeg == 0 then
			self:generateBottomSequence(startSeg, startSeg + segLength, step)
		elseif typeSeg == 1 then
			self:generatePairSequence(startSeg, startSeg + segLength, step)
		elseif typeSeg == 2 then
			self:generateTopSequence(startSeg, startSeg + segLength, step)
		end
		
		startSeg = startSeg + segLength + 4*step
	end

	-- out = ""
	-- tmp = ""
	-- for i, o in ipairs(self.objects) do
	-- 	tmp = string.format("%d|%d|%d|%d;", o.x, o.y, o.width, o.height)
	-- 	out = out..tmp
	-- end
	-- print(out)

	return length, self.objects
end

function Track:generateBottomSequence(start, length, step)
	local height, object
	for x = start, length, step do
		height = self:getRandomHeight()

		object = Object(self.world, x, self.floor - height, 0, step, height)
		table.insert(self.objects, object)
	end
end

function Track:generateTopSequence(start, length, step)
	local height, object
	for x = start, length, step do
		height = self:getRandomHeight()

		object = Object(self.world, x, self.ceil, 0, step, height)
		table.insert(self.objects, object)
	end
end

function Track:generatePairSequence(start, length, step)
	local object, freeSpace, topH, bottomH
	local threshold = self.height / 10
	local boundingHeight = 175
	local pair, prevPair = {}, {}

	prevPair = self:buildPair(start, nil)
	table.insert(self.objects, prevPair.top)
	table.insert(self.objects, prevPair.bottom)

	for x = start + step, length, step do
		pair = self:buildPair(x, prevPair)
		table.insert(self.objects, pair.top)
		table.insert(self.objects, pair.bottom)

		prevPair = pair
	end
end

function Track:buildPair(x, prevPair)
	local pair, boundingHeight, threshold = {}, 200, self.height / 10
	local topH, bottomH, freeSpace, object
	local valid = false
	freeSpace = self:getRandomFreeSpace()
	
	if prevPair then
		repeat
			topH = love.math.random(threshold, self.height - freeSpace - threshold)
			bottomH = self.height - freeSpace - topH

			valid = prevPair.bottom.y - boundingHeight >= self.ceil + topH
			valid = valid and prevPair.top.y + prevPair.top.height + boundingHeight <= self.floor - bottomH
		until valid
	else
		topH = love.math.random(threshold, self.height - freeSpace - threshold)
		bottomH = self.height - freeSpace - topH
	end

	object = Object(self.world, x, self.ceil, 0, step, topH)
	pair.top = object
	
	object = Object(self.world, x, self.floor - bottomH, 0, step, bottomH)
	pair.bottom = object

	return pair
end

function Track:getRandomHeight(force)
	local heightType = force or love.math.random(0, 3)

	if heightType == 0 then
		return self.height * love.math.random(50, 60) / 100
	elseif heightType == 1 then
		return self.height * love.math.random(30, 50) / 100
	elseif heightType == 2 then
		return self.height * love.math.random(15, 30) / 100
	elseif heightType == 3 then
		return self.height * love.math.random(5, 15) / 100
	end
end

function Track:getRandomFreeSpace(force)
	local spaceType = force or love.math.random(0, 4)

	if spaceType == 0 or spaceType == 2 then
		return self.height * love.math.random(35, 40) / 100
	elseif spaceType == 1 or spaceType == 3 then
		return self.height * love.math.random(40, 45) / 100
	elseif spaceType == 4 then
		return self.height * love.math.random(45, 50) / 100
	end
end

function Track:randomRect(x, yMin, yMax, width)
	local height = math.abs(yMax - yMin)
	local y, h, mode

	mode = love.math.random(0, 2)
	if mode == 0 then 		-- 1/3 пространства
		h = height * 0.3
	elseif mode == 1 then -- 1/5 пространства
		h = height * 0.2
	elseif mode == 2 then -- от 1/3 до 3/4 пространства
		h = height * love.math.random(20, 30) / 100
	end
	mode = love.math.random(0, 1)
	if mode == 0 then 		-- на уровне пола
		y = yMin - h
	elseif mode == 1 then -- на уровне потолка
		y = yMax
	end

	return Object(self.world, x, y, 0, width, h)
end


return Track