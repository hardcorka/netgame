
-- функция предзагрузки и инициализации
-- игрового контента, зависимостей и пр.
function love.load()
	local GameManager = require('include/gameManager')
	local Game = require('include/game')
	local Client = require('include/multiplayer')

	window = {
		width = love.window.getWidth(),
		height = love.window.getHeight()
	}

	manager = GameManager()
	game = Game(manager)
	-- client = Client()
	-- client:load()
end

-- игровой цикл, вызывается каждый "тик"
function love.update(dt)
	game:update(dt)
	-- client:update(dt, game.player)
end

-- функция рендеринга игры
function love.draw()
	game:draw()
	-- client:draw()
end

function love.mousepressed(x, y, b)
	game:mousepressed(x, y, b)
end

function love.keypressed(key)
	game:keypressed(key)
end

function love.textinput(text)
	game:textinput()
end
